﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;

using Training.Models;

namespace Training.Controllers
{
    
        [Route("api/[controller]")]
        [ApiController]
        public class StudentController : ControllerBase
        {
            private readonly StudentContext _context;
           
            

            public StudentController(StudentContext context)
            {
                _context = context;
                
                if (_context.Students.Count() == 0)
                {
                    // Create a new TodoItem if collection is empty,
                    // which means you can't delete all TodoItems.
                    _context.Students.Add(new Student { Name = "Item1" });
                    _context.SaveChanges();
                }
            }
           

            

            [HttpGet]
            public async Task<ActionResult<IEnumerable<Student>>> GetStudent()
            
            { 
                return await _context.Students.ToListAsync();
            }


            [HttpGet("{id}")]
            public async Task<ActionResult<Student>> GetStudent(long id)
            {
                var student = await _context.Students.FindAsync(id);

                if (student == null)
                {
                    return NotFound();
                }

                return student;
            }
            
           
            
            [HttpPost]
            public async Task<ActionResult<Student>> CreateStudent(Student item)
            
            {          
               var student =await _context.Students.FindAsync(item.Id);
                if (student != null)
                    return StatusCode((int)HttpStatusCode.BadRequest,("ID already exists")); 
                var query = _context.Students
                    .Where(s => s.Name == item.Name)
                    .FirstOrDefault<Student>();
                if (query != null)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest,("Name already exists")); 
                }
                try
                {
                    _context.Students.Add(item);
                    await _context.SaveChangesAsync();
                    return CreatedAtAction(nameof(GetStudent), new { id = item.Id }, item);
                }
                catch (Exception e)
                {
                    
                    return StatusCode((int)HttpStatusCode.BadRequest, e); 
                }
                

                
            }

            // PUT api/values/5
            [HttpPut("{id}")]
            public async Task<IActionResult> PutStudent(long id, Student item)
            {
                if (id != item.Id)
                {
                    return BadRequest();
                }

                _context.Entry(item).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return NoContent();
            }
            [HttpDelete("{id}")]
            public async Task<IActionResult> DeleteStudent(long id)
            {
                var student = await _context.Students.FindAsync(id);

                if (student == null)
                {
                    return NotFound();
                }

                _context.Students.Remove(student);
                await _context.SaveChangesAsync();

                return NoContent();
            }
           
        }
    
}