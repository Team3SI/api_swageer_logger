﻿using System.ComponentModel.DataAnnotations;

namespace Training.Models
{
    public class Student
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Range(18,60)]
       public int Age { get; set; }
        [EmailAddress(ErrorMessage ="Please enter a valid email")]
        public string EmailID {get;set;}

        
    }
}